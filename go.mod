module gitlab.com/flex_comp/i18n

go 1.16

require (
	gitlab.com/flex_comp/comp v0.1.3 // indirect
	gitlab.com/flex_comp/log v0.0.0-20210729190650-519dfabf348e // indirect
	gitlab.com/flex_comp/util v0.0.0-20210813015552-fa5a0e253e1a // indirect
)
