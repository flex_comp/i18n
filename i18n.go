package i18n

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/util"
	"sync"
)

var (
	I18n *I18N
)

func init() {
	I18n = new(I18N)
	I18n.raw = make(map[LangCode]words)
	_ = comp.RegComp(I18n)
}

type common struct {
	Default LangCode `json:"default"`
	Used    LangCode `json:"used"`
}

type words map[string]interface{}
type I18N struct {
	raw         map[LangCode]words
	defaultLang LangCode

	com *common
	mtx sync.RWMutex
}

func (i *I18N) Init(args map[string]interface{}, _ ...interface{}) error {
	dir := DefaultDir
	if d, ok := args["lang_dir"]; ok {
		dir = util.ToString(d)
	}

	m, err := util.LoadFromDir(dir, "json")
	if err != nil {
		return err
	}

	i.mtx.Lock()
	defer i.mtx.Unlock()
	for k, v := range m {
		switch k {
		case "common":
			i.com = new(common)
			_ = util.JSONUnmarshal([]byte(v), i.com)
		default:
			wds := make(map[string]interface{})
			_ = util.JSONUnmarshal([]byte(v), &wds)
			i.raw[LangCode(k)] = wds
		}
	}

	return nil
}

func (i *I18N) Start(...interface{}) error {
	return nil
}

func (i *I18N) UnInit() {

}

func (i *I18N) Name() string {
	return "i18n"
}

func SetLang(lang string) {
	I18n.SetLang(lang)
}

func (i *I18N) SetLang(lang string) {
	i.mtx.Lock()
	defer i.mtx.Unlock()

	i.com.Used = LangCode(lang)
}

func Lang() string {
	return I18n.Lang()
}

func (i *I18N) Lang() string {
	i.mtx.RLock()
	defer i.mtx.RUnlock()

	return string(i.com.Used)
}

func Str(key string, args ...interface{}) string {
	return I18n.Str(key, args...)
}

func (i *I18N) Str(key string, args ...interface{}) string {
	i.mtx.RLock()
	wds, ok := i.raw[i.com.Used]
	i.mtx.RUnlock()

	if !ok {
		wds = i.raw[i.com.Default]
	}

	wd, ok := wds[key]
	if !ok {
		wd = "not translated"
	}

	if len(args) > 0 {
		wd = fmt.Sprintf(wd.(string), args...)
	}

	return wd.(string)
}
