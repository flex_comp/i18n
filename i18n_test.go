package i18n

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

func TestI18N(t *testing.T) {
	t.Run("i18n", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{"lang_dir": "./lang_pack"})
		ch := make(chan bool)
		go func() {
			<-time.After(time.Second * 5)
			ch <- true
		}()

		go func() {
			<-time.After(time.Second * 1)
			t.Log(Str("hi"))
			SetLang("en_US")
			t.Log(Str("hi"))
			SetLang("zh_CN")
			t.Log(Str("hi"))
			SetLang("ja_JP")
			t.Log(Str("hi"))

			SetLang("en_US")
			t.Log(Str("hi,sir", "alice"))
			SetLang("zh_CN")
			t.Log(Str("hi,sir", "张三"))
			SetLang("ja_JP")
			t.Log(Str("hi,sir", "田中"))
		}()
		_ = comp.Start(ch)
	})
}
